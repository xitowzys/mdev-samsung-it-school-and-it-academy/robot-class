package xitowzys.robotClass

class Robot(
    var x: Int,
    var y: Int,
    var direction: Direction
) {

    fun turnLeft() {
        direction = when (direction) {
            Direction.RIGHT -> Direction.UP
            Direction.LEFT -> Direction.DOWN
            Direction.DOWN -> Direction.RIGHT
            Direction.UP -> Direction.LEFT
        }
    }

    fun turnRight() {
        direction = when (direction) {
            Direction.RIGHT -> Direction.DOWN
            Direction.LEFT -> Direction.UP
            Direction.DOWN -> Direction.LEFT
            Direction.UP -> Direction.RIGHT
        }
    }

    fun stepForward() {
        when (direction) {
            Direction.RIGHT -> x++
            Direction.LEFT -> x--
            Direction.DOWN -> y--
            Direction.UP -> y++
        }
    }

    override fun toString(): String {
        return "x: $x, y: $y, dir: $direction"
    }

}